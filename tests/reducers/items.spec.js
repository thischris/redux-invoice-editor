import items from '../../client/reducers/items'

describe('addItems reducer', () => {
    it('should return the initial state', () => {
        expect(
            items(undefined, {})
        ).toEqual([])
    });
    it('should handle ADD_ITEM', () => {
        expect(
            items([{item: "Test", qty: 1, price: 10}], 'ADD_ITEM')
        ).toEqual([
            {
                item: "Test",
                qty: 1,
                price: 10.00
            }
        ]);
        expect(
            items(
                [
                    {
                        item: "Widget",
                        qty: 2,
                        price: 10.00
                    },
                    {
                        item: "Cog",
                        qty: 2,
                        price: 15.99
                    }
                ], {
                    type: 'ADD_ITEM',
                    item: "Test",
                    qty: 1,
                    price: 10.00
                }
            )
        ).toEqual([
            {
                item: "Widget",
                qty: 2,
                price: 10.00
            },
            {
                item: "Cog",
                qty: 2,
                price: 15.99
            },
            {
                item: "Test",
                qty: 1,
                price: 10.00
            }
        ])
    });
    it('should handle REMOVE_ITEM', () => {
        expect(
            items([
                {
                    item: "Widget",
                    qty: 2,
                    price: 10.00
                },
                {
                    item: "Cog",
                    qty: 2,
                    price: 15.99
                },
                {
                    item: "Test",
                    qty: 1,
                    price: 10.00
                }
            ], {
                type: 'REMOVE_ITEM',
                item: "Test",
                i: 2
            })
        ).toEqual([
            {
                item: "Widget",
                qty: 2,
                price: 10.00
            },
            {
                item: "Cog",
                qty: 2,
                price: 15.99
            }
        ])
    });
});