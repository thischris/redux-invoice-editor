import React from 'react';

const Item = React.createClass({
    render() {
        return (
            <tbody>
                <tr className="item">
                    <td>{this.props.item.item}</td>
                    <td>{this.props.item.qty}</td>
                    <td>${parseFloat((this.props.item.price) * 100 / 100).toFixed(2)}</td>
                    <td>${parseFloat((this.props.item.price * this.props.item.qty) * 100 / 100).toFixed(2)}</td>
                    <td className="actions-col">
                        <a className="remove-item" onClick={this.props.removeItem.bind(null, this.props.item, this.props.i)} type="submit">&times;</a>
                    </td>
                </tr>
            </tbody>
        )
    }
});

export default Item;