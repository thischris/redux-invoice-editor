import React from 'react';
import Item from './Item';
import AddItem from './AddItem';

const Items = React.createClass({
    render() {
        const subTotal = parseFloat((this.props.items.reduce((obj, item)=>{
                return obj + (item.price * item.qty);
            }, 0)) * 100 / 100).toFixed(2);

        const tax = parseFloat((subTotal * 0.05) * 100 / 100).toFixed(2);
        const total = ((parseFloat(subTotal) + parseFloat(tax)) * 100 / 100).toFixed(2);
        return (
            <div className="invoice-box">
                <table>
                    <thead>
                    <tr className="heading">
                        <th>Item</th>
                        <th>Qty</th>
                        <th>Price</th>
                        <th>Total</th>
                        <th></th>
                    </tr>
                    </thead>
                    {this.props.items.map((item, i) => <Item {...this.props} key={i} i={i} item={item} />)}
                </table>
                <AddItem {...this.props}/>
                <table>
                    <tbody>
                    <tr className="total">
                        <td className="two-col-span" colSpan={2}/>
                        <td>Subtotal</td>
                        <td>${subTotal}</td>
                        <td className="one-col-span" colSpan={1}/>
                    </tr>
                    <tr className="total">
                        <td className="two-col-span" colSpan={2}/>
                        <td>Tax (5%)</td>
                        <td>${tax}</td>
                        <td className="one-col-span" colSpan={1}/>
                    </tr>
                    <tr className="total">
                        <td className="two-col-span" colSpan={2}/>
                        <td>Total</td>
                        <td>${total}</td>
                        <td className="one-col-span" colSpan={1}/>
                    </tr>
                    </tbody>
                </table>
            </div>
        )
    }
});

export default Items;


