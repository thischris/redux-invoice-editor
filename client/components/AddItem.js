import React from 'react';

const AddItem = React.createClass({
    handleSubmit(e){
        e.preventDefault();
        const item = this.refs.item.value;
        const qty= this.refs.qty.value || 1;
        const price= this.refs.price.value;
        this.props.addItem(item, qty, price);
        this.refs.itemForm.reset();
    },
    render() {
        return (
            <form ref="itemForm" className="item-form" onSubmit={this.handleSubmit}>
                <table>
                    <tbody>
                    <tr className="item last">
                        <td>
                            <input type="text" ref="item"/>
                        </td>
                        <td>
                            <input type="number" ref="qty"/>
                        </td>
                        <td>
                            <input type="number" step="0.01" ref="price"/>
                        </td>
                        <td>
                            <input type="number" disabled="disabled" ref="total"/>
                        </td>
                        <td className="actions-col">
                            <input type="submit" hidden />
                            <a className="add-item" onClick={this.handleSubmit} type="submit">&#8250;</a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        )
    }
});

export default AddItem;