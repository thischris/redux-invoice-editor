import React from 'react';
import Items from './Items'

const Main = React.createClass({
  render() {
    return (
      <div className="invoice-container">
          <Items {...this.props}/>
      </div>
    )
  }
});

export default Main;
