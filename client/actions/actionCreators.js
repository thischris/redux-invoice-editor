// add item
export function addItem(item, qty, price) {
    return {
        type: 'ADD_ITEM',
        item,
        qty,
        price
    }
}

// remove item
export function removeItem(item, i) {
    return {
        type: 'REMOVE_ITEM',
        item,
        i
    }
}