function items(state = [], action) {
    switch (action.type) {
        case 'ADD_ITEM':
            return[...state, {
                item: action.item,
                qty: action.qty,
                price: action.price
            }];
        case 'REMOVE_ITEM':
            return[
                ...state.slice(0, action.i),
                ...state.slice(action.i + 1)
            ];
        default:
            return state;
    }
    return state;
}

export default items;
