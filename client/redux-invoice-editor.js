import React from 'react';

import { render } from 'react-dom';

// Import css
import css from './styles/style.styl';

// Import Components
import App from './components/App';

import store from './store';


render(<App store={store}/>, document.getElementById('root'));