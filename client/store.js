import { createStore } from 'redux';
// import the root reducer
import rootReducer from './reducers/index';
import items from './data/items';

// create an object for the default data
const defaultState = {items};

const store = createStore(rootReducer, defaultState);

export default store;
