# Redux Invoice Editor

An invoice editor that allows a user to add, edit, or remove line items. 
Pricing updated on-the-fly as line items are added or edited.

Used 'starter-kit' files from https://github.com/wesbos/Learn-Redux

## Running

First `npm install` to grab all the necessary dependencies. 

Then run `npm start` and open <localhost:7777> in your browser.


## Testing

Run `npm test`